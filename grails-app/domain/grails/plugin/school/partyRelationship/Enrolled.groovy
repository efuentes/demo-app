package grails.plugin.school.partyRelationship

import grails.plugin.core.party.PartyRelationship

class Enrolled extends PartyRelationship {

  String toString() {
    "Enrolled"
  }

}