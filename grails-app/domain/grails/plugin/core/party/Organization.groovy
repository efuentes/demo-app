package grails.plugin.core.party

class Organization extends Party {

  String name

  boolean restricted = false
  Date dateCreated = new Date()
  Date lastUpdated = new Date()

  static mapping = {
    sort name: 'desc' 
  }

  static constrains = {
    name (nullable: false, blank: false)
    restricted (nullable: true)
    dateCreated (nullable: true)
    lastUpdated (nullable: true)
  }

  String toString() {
    name
  }

}