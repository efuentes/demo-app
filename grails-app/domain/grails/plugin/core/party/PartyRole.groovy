package grails.plugin.core.party

class PartyRole implements Serializable {
  
  static belongsTo = [party: Party]

/*  static hasMany = [
    partyRelationships: PartyRelationship
  ]*/

  Date fromDate
  Date thruDate

  boolean restricted = false
  Date dateCreated = new Date()
  Date lastUpdated = new Date()

  static constrains = {
    fromDate (nullable: true)
    thruDate (nullable: true)
    restricted (nullable: true)
    dateCreated (nullable: true)
    lastUpdated (nullable: true)
  }

}