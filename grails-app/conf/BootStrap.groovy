import grails.plugin.core.auth.*
import grails.plugin.core.menu.*

import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {

	def userService
	def adminService

	def init = { servletContext ->

		def permission

		permission = new Permission(module: 'Auth',
																title: 'Administer permissions',
																description: 'Give to trusted roles only; this permission has security implications.',
																domain: 'permission',
																actions: '*',
																instances: '*',
																restricted: true)
		if (!permission.save()) {
			log.error "Unable to create permission ${permission.title}."
			permission.errors.each { log.error it }

			throw new RuntimeException("Unable to create permission ${permission.title}.")
		}

		permission = new Permission(module: 'Auth',
																title: 'Administer users',
																description: 'Give to trusted roles only; this permission has security implications.',
																domain: 'user',
																actions: '*',
																instances: '*',
																restricted: true)
		if (!permission.save()) {
			log.error "Unable to create permission ${permission.title}."
			permission.errors.each { log.error it }

			throw new RuntimeException("Unable to create permission ${permission.title}.")
		}

		permission = new Permission(module: 'Auth',
																title: 'Administer roles',
																description: 'Give to trusted roles only; this permission has security implications.',
																domain: 'role',
																actions: '*',
																instances: '*',
																restricted: true)
		if (!permission.save()) {
			log.error "Unable to create permission ${permission.title}."
			permission.errors.each { log.error it }

			throw new RuntimeException("Unable to create permission ${permission.title}.")
		}

/*		def assetsPermission
		assetsPermission = new Permission(module: 'Auth',
																title: 'Assets',
																description: 'This permission must be given to all roles to have access to all assets.',
																domain: 'assets',
																actions: '*',
																instances: '*',
																restricted: true)
		if (!assetsPermission.save()) {
			log.error "Unable to create assetsPermission ${assetsPermission.title}."
			assetsPermission.errors.each { log.error it }

			throw new RuntimeException("Unable to create assetsPermission ${assetsPermission.title}.")
		}*/

		def userRole = Role.findByName(UserService.USER_ROLE)
		if (!userRole) {
			userRole = new Role(description: 'Assigned to users who has been authenticated',
			                    name: UserService.USER_ROLE,
			                    restricted: true)
			//userRole.addToPermissions(assetsPermission)
			userRole.save()

			if (userRole.hasErrors()) {
				userRole.errors.each { log.error(it) }
				throw new RuntimeException("Unable to create valid users role")
			}
		}

		def adminRole = Role.findByName(AdminService.ADMIN_ROLE)
		if (!adminRole) {
			adminRole = new Role(description: 'Assigned to users who are considered to be system wide administrators',
			                     name: AdminService.ADMIN_ROLE,
			                     restricted: true)
			adminRole.save()

			if (adminRole.hasErrors()) {
				adminRole.errors.each { log.error(it) }
				throw new RuntimeException("Unable to create valid administrative role")
			}
		}

		// Assign role user to assets permission
/*		assetsPermission.addToRoles(userRole)
		assetsPermission.save()*/

		if(!User.findByUsername("user")) {
			// Create example User account
			def user = new User (username: 'user',
													passwordHash: new Sha256Hash("useR123!").toHex(),
													name: 'Test User',
													email: 'test@mail.com',
													enabled: true )

/*			def userProfile = new Profile(firstName: 'Test User',
																		owner: user)
			user.profile = userProfile*/

			log.info("Creating default user account with username: user")

			user.addToRoles(userRole)

			def savedUser = user.save()
			if (savedUser) {
				userRole.addToUsers(savedUser)
				userRole.save()

				if (userRole.hasErrors()) {
					log.error("Unable to assign default role to new user [$savedUser.id]$savedUser.username")
					userRole.errors.each { log.error(it) }

					throw new RuntimeException("Unable to assign default role to new user [$savedUser.id]$savedUser.username")
				}
				log.info("Successfully created user [$user.id]$user.username")
			} else {
				log.debug("Submitted details for new user account are invalid")
				user.errors.each { log.debug it }
				throw new RuntimeException("Unable to create the default user account")
			}
		}

		if(!User.findByUsername("admin")) {
			// Create example Administrative account
			def admins = Role.findByName(AdminService.ADMIN_ROLE)

			def admin = new User (username: 'admin',
														passwordHash: new Sha256Hash("admiN123!").toHex(),
														name: 'Administrator',
														email: 'admin@mail.com',
														enabled: true,
														restricted: true )

/*			def adminProfile = new Profile( firstName: 'Administrator',
																			owner: admin)
			admin.profile = adminProfile*/

			log.info("Creating default admin account with username: admin")

			adminRole.addToUsers(admin)
			admin.addToRoles(adminRole)

			if (!adminRole.save()) {
				log.error "Unable to grant administration privilege to [$admin.id]:$admin.username"
				adminRole.errors.each {  log.error '[${admin.username}] - ' + it }
				adminRole.discard()
				admin.discard()
				throw new RuntimeException("Unable to grant administration privilege to [$admin.id]:$admin.username")
			}

			if (!admin.save()) {
				log.error "Unable to grant administration role to [$admin.id]$admin.username failed to modify admin account"
				admin.errors.each { log.error it }

				throw new RuntimeException("Unable to grant administration role to [$admin.id]$admin.username")
			}

			log.info "Granted administration privileges to [$admin.id]:$admin.username"
		}

		def mainMenu = new Menu(module: 'core',
														name: 'menu.core.main',
														title: 'Main Menu',
														permission: '*',
														restricted: true)

		if (!mainMenu.save()) {
			log.error "Unable to create Main Menu."
			mainMenu.errors.each {  log.error '[${mainMenu.name}] - ' + it }
			mainMenu.discard()
			throw new RuntimeException("Unable to grant administration privilege to [$mainMenu.id]:$mainMenu.name")
		}

	}

  def destroy = {
	}

}
