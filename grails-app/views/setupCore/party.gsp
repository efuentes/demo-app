<h1>Party Loaded</h1>
<br>
<h2>Gender</h2>
<table>
  <tbody>
    <tr>
      <th>description</th>
      <th>restricted</th>
    </tr>
    <g:each in="${json.genders}" var="gender">
      <tr>
        <td>${gender.description}</td>
        <td>${gender.restricted}</td>
      </tr>
    </g:each>
  </tbody>
</table>

<g:link controller="gender" action="index">Gender List</g:link>
