package grails.plugin.core

import grails.plugin.core.party.*
import grails.plugin.core.menu.*

import grails.converters.JSON

class SetupCoreController {

  def index() {

  }

  def auth() {
    println "auth: ${params}"
    
    def file = request.getFile('jsonAuthUpload')
    //println file.inputStream.text
    println "File content type: " + file.getContentType()
    
    def okcontents = ['application/json', 'application/octet-stream']
    
    if (! okcontents.contains(file.getContentType())) {
      flash.message = "File must be one of: ${okcontents}, and was received: [${file.getContentType()}]"
      render(view: 'index')
      return;
    }
    
    def json = JSON.parse(file.getInputStream(),"UTF-8")
    
    println json

    // Menus
    json.menus.each { menu->
      def parent

      if (menu.parent) {
        parent = Menu.findByName(menu.parent)
      }

      new Menu(module: menu.module,
               name: menu.name,
               title: menu.title,
               description: menu.description,
               icon: menu.icon,
               permission: menu.permission,
               parent: parent
              ).save(failOnError: true)
    }
    
    json.items.each { item->
      def menu = Menu.findByName(item.menu)

      new MenuItem(name: item.name,
                   title: item.title,
                   description: item.description,
                   icon: item.icon,
                   permission: item.permission,
                   linkController: item.linkController,
                   linkAction: item.linkAction,
                   menu: menu,
                   weight: item.weight
                  ).save(failOnError: true)
    }

    [json: json]
  }

  def party() {
    println "party: ${params}"
    
    def file = request.getFile('jsonPartyUpload')
    //println file.inputStream.text
    println "File content type: " + file.getContentType()
    
    def okcontents = ['application/json', 'application/octet-stream']
    
    if (! okcontents.contains(file.getContentType())) {
      flash.message = "File must be one of: ${okcontents}, and was received: [${file.getContentType()}]"
      render(view: 'index')
      return;
    }
    
    def json = JSON.parse(file.getInputStream(),"UTF-8")
    
    println json

    // Genders
    json.genders.each { gender->
      new Gender(description: gender.description,
                 restricted: gender.restricted
                ).save(failOnError: true)
    }

    [json: json]
  }

}